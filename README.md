# README #

Version 0.1
Pibot is a library for setting up the raspberry pi as an Alexa/OpenCV/PiClap enabled web enabled rover.

### How do I get set up? ###

This project relies on the following dependencies

* [bottle](http://bottlepy.org/docs/dev/tutorial.html)

* [raspirobot](http://www.monkmakes.com/rrb3/)

* [pi-clap](https://github.com/nikhiljohn10/pi-clap)

* [alexa-avs-raspberry-pi](https://github.com/amzn/alexa-avs-raspberry-pi)