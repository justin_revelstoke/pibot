from bottle import route, run, template, request
from subprocess import call
import rrb3 as rrb
import time


# Change these for your setup.
IP_ADDRESS = '192.168.1.117' # of your Pi
PORT = 8081
BATTERY_VOLTS = 9
MOTOR_VOLTS = 6

# Configure the RRB
rr = rrb.RRB3(BATTERY_VOLTS, MOTOR_VOLTS)

@route('/startstream')
def index():
    call(["mkdir", "/tmp/stream"])
    call(["raspistill", "-w", "320", "-h", "240", "-q", "5", "-o", "/tmp/stream/pic.jpg", "-tl", "250", "-t", "9999999", "-th", "0:0:0"])
    call(["LD_LIBRARY_PATH=/usr/local/lib", "mjpg_streamer", "-i", "'input_file.so -f /tmp/stream -n pic.jpg'", "-o", "'output_http.so -w /usr/local/www'"])
    return template('')

@route('/')
def index():
    cmd = request.GET.get('command', '')
    if cmd == 'w':
        rr.forward()
    elif cmd == 'a':
        rr.left(0, 0.5) # turn at half speed
    elif cmd == 'd':
        rr.right(0, 0.5)
    elif cmd == 's':
        rr.reverse(0, 0.3) # reverse slowly
    else:
        rr.stop()
    return template('home.tpl')
        
try: 
    run(host=IP_ADDRESS, port=PORT)
finally:  
    rr.cleanup()
